
from shop import db, login_manager
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin


class Item(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    item_name = db.Column(db.String(50),nullable=False)
    description = db.Column(db.String(120), nullable=False)
    description_extended = db.Column(db.String(600), nullable=False)
    price = db.Column(db.Numeric(10,2), nullable=False)
    image_file = db.Column(db.String(30), nullable=False, default='default.jpg')

    def __repr__(self):
        return f"Item('{self.item_name}', '{self.description_1}', '{self.description_2}', '{self.price}')"


class Checkout(db.Model):
    id = db.Column(db.Integer(), primary_key = True)
    total_price = db.Column(db.Integer())
    street = db.Column(db.String(100), nullable = False)
    city = db.Column(db.String(30), nullable = False)
    card_name = db.Column(db.String(20), nullable = False)
    card_no = db.Column(db.String(16), nullable = False)
    card_exp_date = db.Column(db.String(4), nullable = False)
    card_sec = db.Column(db.String(3), nullable = False)



class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(25), unique=True, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    password_hash = db.Column(db.String(128))
    password = db.Column(db.String(60), nullable=False)

    def __repr__(self):
        return f"User('{self.username}', '{self.email}')"

    @property
    def password(self):
        raise AttributeError('password is not a readable attribute')

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)



@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))