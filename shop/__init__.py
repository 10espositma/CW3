from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager


app = Flask(__name__)
app.config['SECRET_KEY'] = 'cbb3a8bdf2dd568355680378c61aa407ca3f1eb37e395992'

app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://c1845089:M9MM9KSY5SaAbGe@csmysql.cs.cf.ac.uk:3306/c1845089'


db = SQLAlchemy(app)

login_manager = LoginManager()
login_manager.init_app(app)

from shop import routes