from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, BooleanField
from wtforms.validators import DataRequired, Length, Email, EqualTo, ValidationError, Regexp
from shop.models import User

class RegistrationForm(FlaskForm):
	username = StringField('Username', validators=[DataRequired(), Length(min=3, max=15)])
	email = StringField('Email', validators=[DataRequired(), Email()])
	password = PasswordField('Password', validators=[DataRequired(), Regexp('((?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,15})',
	 message='Your password should be between 5 and 15 characters long; contain at least one UPPER and LOWER case letter and one digit.')])
	confirm_password = PasswordField('Confirm Password', validators=[DataRequired(), EqualTo('password', message = 'Passwords must match.')])
	submit = SubmitField('Register')

	def validate_username(self, username):
		user = User.query.filter_by(username=username.data).first()
		if user:
			raise ValidationError('Username already exists. \
				Please choose another or login.')

	def validate_email(self, email):
		user = User.query.filter_by(email = email.data).first()
		if user:
			raise ValidationError('This email is already registered. \
				Please choose another or login.')


class LoginForm(FlaskForm):
	email = StringField('Email', validators=[DataRequired(), Email()])
	password = PasswordField('Password', validators=[DataRequired()])
	submit = SubmitField('Login')


class CheckoutForm(FlaskForm):
	street = StringField('Street', validators=[DataRequired()])
	city = StringField('City', validators=[DataRequired()])
	card_name = StringField('Card Name', validators=[DataRequired(), Length(min=3, max=30)])
	card_no = StringField('Card Number', validators=[DataRequired(), Length(min=16, max=16)])
	card_exp_date = StringField('Card Expiry', validators=[DataRequired(), Length(min = 4, max=4)])
	card_sec = StringField('Security Number', validators=[DataRequired(), Length(min = 3, max=3)])
	submit = SubmitField('Purchase')