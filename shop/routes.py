import os
from flask import render_template, url_for, request, redirect, flash, Flask, session
from shop import app, db
from shop.models import User, Item, Checkout
from shop.forms import RegistrationForm, LoginForm, CheckoutForm
from flask_login import login_user, current_user, logout_user, login_required

@app.route("/")
@app.route("/home")
def home():
    items = Item.query.all()
    return render_template('home.html', title='Home', items = items)


@app.route("/about")
def about():
    return render_template('about.html', title='About')

@app.route("/item/<int:item_id>")
def item(item_id):
	item = Item.query.get_or_404(item_id)
	return render_template('item.html', title=item.item_name, item=item)

@app.route("/register", methods=['GET', 'POST'])
def register():
	form = RegistrationForm()
	if form.validate_on_submit():
		user = User(username=form.username.data, email=form.email.data, password=form.password.data)
		db.session.add(user)
		db.session.commit()
		flash('Your account has been created. You can now log in.')
		return redirect(url_for('home'))
	return render_template('register.html', title='Register', form=form)




@app.route("/login", methods=['GET', 'POST'])
def login():
	form = LoginForm()
	if form.validate_on_submit():
		user = User.query.filter_by(email=form.email.data).first()
		if user is not None and user.verify_password(form.password.data):
			login_user(user)
			flash('You are now logged in.')
			return redirect(url_for('home'))
		flash('Invalid username or password.')
		return render_template('login.html', form=form)
	return render_template('login.html', title='Login', form=form)



@app.route("/logout")
def logout():
	logout_user()
	return redirect(url_for('home'))


@app.route("/add_to_cart/<int:item_id>")
def add_to_cart(item_id):
	if "cart" not in session:
		session["cart"] = []
	session["cart"].append(item_id)
	flash("Item added to your shopping cart!")
	return redirect("/cart")




@app.route("/cart", methods=['GET', 'POST'])
def shopping_cart():    
    if "cart" not in session:
        flash("There is nothing in your cart.")
        return render_template("cart.html", display_cart = {}, total = 0)
    else:
        items = session["cart"]
        cart = {}

        total_price = 0
        total_quantity = 0
        for i in items:
            item = Item.query.get_or_404(i)
            total_price += item.price
            if item.id in cart:
                cart[item.id]["total_quantity"] += 1
            else:
                cart[item.id] = {"total_quantity":1, "title": item.item_name, "price":item.price}
            total_quantity = sum(i['total_quantity'] for i in cart.values())
        
    return render_template("cart.html", display_cart = cart, total = total_price)



@app.route("/delete_item/<int:item_id>", methods=['POST'])
def delete_item(item_id):
	if "cart" not in session:
		session["cart"] = []
	session["cart"].remove(item_id)

	flash("This item has been removed from your shopping cart.")
	session.modified = True
	return redirect("/cart")



@app.route("/checkout", methods=["GET", "POST"])
def checkout():
	form = CheckoutForm()
	if form.validate_on_submit():
		order = Checkout(street = form.street.data, city = form.city.data, \
			card_name = form.card_name.data, card_no = form.card_no.data, \
			card_exp_date = form.card_exp_date.data, card_sec = form.card_sec.data)
		db.session.add(order)
		db.session.commit()
		flash('Your order has been successful.')
		return redirect(url_for('home'))
	return render_template("checkout.html", title = "Checkout", form = form)